<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
    </style>
    <div class="container" style="padding: 30px; 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-healding">
                        <div class="row">
                            <div class="col md 12">
                                <a href="{{ route('admin.addhomeslider') }}" class="btn btn-success pull-right" style="    margin-right: 20px;
                            }">Add New Slider</a>
                            </div>
                            <h3 style="text-align: center; color:rgb(255, 0, 0)">All Slider List</h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        {{-- @if (Session::has('message'))
                        <div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
                        @endif --}}
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>SubTitle</th>
                                        <th>Price</th>
                                        <th>Link</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sliders as $slider)
                                        <tr>
                                            <td>{{ $slider->id }}</td>
                                            <td><img src="{{ asset('assets/images/sliders') }}/{{ $slider->image }}" width="120px;"></td>
                                            <td>{{ $slider->title }}</td>
                                            <td>{{ $slider->subtitle }}</td>
                                            <td>TK{{ $slider->price }}</td>
                                            <td>{{ $slider->link }}</td>
                                            <td>{{ $slider->status ==1 ? 'active':'inactive' }}</td>
                                            <td>{{ $slider->created_at }}</td>
                                            <td>
                                               {{-- <a href="{{ route('admin.editproduct',['product_slug'=>$product->slug]) }}"><i class="fa fa-edit fa-2x text-warning"></i> Edit </a>
                                               <a href="#" style="margin-left: 10px;" wire:click.prevent="deleteCategory({{ $product->id }})"><i class="fa fa-times fa-2x text-danger"></i>Delete</a> --}}
                                            </td>
                                        </tr>                                    
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- {{ $sliders->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
